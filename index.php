<?php

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);

error_reporting(E_ALL & ~E_NOTICE ^ E_DEPRECATED);

class txValidator {	
	
	#Private
	private $_usersData; 

	private $_anonimousData;
	
	#to save resources on opening json cycle all transaction :)	
	private $_userTempData;	

    public function __construct($data = array()) {						
        $this->_usersData=$data['usersData']; #handled has json object					

		$this->_userTempData=[];
    }
	
	public function txsProcessNew(string $data) {		
		
		$_txs=json_decode($data);	#handled has json object			

		#init anonimous data collected - temporary for debug only
		$this->_anonimousData=new stdClass();
		$this->_anonimousData->sum=0; #total amount
		$this->_anonimousData->txs=0; #number of transactions found
		
		$this->_anonimousData->min=0; #for min val
		$this->_anonimousData->max=0; #for max val

		
		$tx=[]; #new array for each transaction
		
		foreach($_txs->transactions as $tx) { //foreach element in $_txs		
			
			#check if valid transaction 
			if ($tx->confirmations > 5 ) {	

				#check min & max
				$this->_anonimousData->min= $this->_anonimousData->min < $tx->amount ? $this->_anonimousData->min : $tx->amount;
				$this->_anonimousData->max= $this->_anonimousData->max > $tx->amount ? $this->_anonimousData->max : $tx->amount;
			
				#user exist?
				if (property_exists($this->_usersData->users, $tx->address)) {
					
					#init temporary user if not exist
					if (!array_key_exists ($tx->address , $this->_userTempData )) {
						$this->_userTempData[$tx->address]=new stdClass();
						$this->_userTempData[$tx->address]->name=$this->_usersData->users->{$tx->address}->name;
						$this->_userTempData[$tx->address]->txSUM=0;												
						$this->_userTempData[$tx->address]->txCounter=0;
					}
					
					
					#check transaction DOES NOT exist & add it
					if (!property_exists($this->_usersData->users->{$tx->address}->txs, $tx->txid)) {						
						$this->_usersData->users->{$tx->address}->txs->{$tx->txid}=new stdClass();
						
						$this->_usersData->users->{$tx->address}->txs->{$tx->txid}->category=$tx->category;
						$this->_usersData->users->{$tx->address}->txs->{$tx->txid}->amount=$tx->amount;						
						
						/*
						using temporary user sum here & mapping database schema 						
						init users data temporary & using array object index by reference										
						*/
						$this->_userTempData[$tx->address]->txSUM += $tx->amount;						
						$this->_userTempData[$tx->address]->txCounter++;						
					}

				}
				#anonimous user
				else {
					$this->_anonimousData->sum += $tx->amount;	
					$this->_anonimousData->txs++;					
				}		
			
			}
		}
		
		
		#save database - new json
		file_put_contents("users_updated.json", json_encode($this->_usersData));			
		
		#output data
		$this->outputData();		
		
		
	}
	
	private function outputData() {
		
		echo "``` <br>";
		
		
		#loop here data from users temporary 
		foreach($this->_userTempData as $wallet_address=>$userTempData) {		

			echo sprintf("Deposited for %s: count=%s sum=%s <br>",
						$userTempData->name,
						$userTempData->txCounter,
						$this->convertFloat($userTempData->txSUM)
						);				
		
		}
		
		
		echo sprintf("Deposited without reference: count=%s sum=%s <br>",
					$this->_anonimousData->txs,
					$this->convertFloat($this->_anonimousData->sum)
					);	
		echo sprintf("Smallest valid deposit: %s <br>", $this->_anonimousData->min);
		echo sprintf("Largest valid deposit: %s <br>", $this->_anonimousData->max);
		echo "```";
	}	
	
    private function convertFloat($floatAsString)
    {
        $norm = strval(floatval($floatAsString));
    
        if (($e = strrchr($norm, 'E')) === false) {
            return $norm;
        }
    
        return number_format($norm, -intval(substr($e, 1)));
    }	
	
}


#init object array
$data=[];

#DATABASE - load json file & handle json has object
$data["usersData"]=json_decode(file_get_contents("users.json"));				

#init object paymentValidator - add to construtor array with data
$txVal=new txValidator($data);


#TRANSACTIONS - load "new transactions" json file & handle json has object & process transactions
$txVal->txsProcessNew(file_get_contents("txs.json"));


?>
